// import logo from "./logo.svg";
import "./App.css";
import TodoPage from "./pages/todoPage";

function App() {
  return (
    <div className="App">
      <TodoPage />
    </div>
  );
}

export default App;
