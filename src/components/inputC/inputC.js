import { Button, TextField } from "@mui/material";
import React, { useContext } from "react";
import { MyContext } from "../../store/context";
import "./inputStyle.css";

function InputC() {
  const { task, setTask, Id, setTodoItems } = useContext(MyContext);
  return (
    <div className="inputDiv">
      {/* <input type="text" name="task" value={task} /> */}
      <TextField
        id="outlined-basic"
        placeholder="Add Your Task Here"
        // label="Add Your Task Here"
        variant="outlined"
        size="small"
        value={task}
        onInput={(e) => setTask(e.target.value)}
      />
      {/* taskName */}
      <Button
        variant="contained"
        onClick={() =>
          setTodoItems({
            taskName: task,
            taskId: Id,
            complated: false,
          })
        }
      >
        Add Task
      </Button>
    </div>
  );
}

export default InputC;
