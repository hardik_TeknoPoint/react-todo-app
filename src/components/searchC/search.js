import React, { useContext, useState } from "react";
import { Input, Button } from "@mui/material";
import { Search } from "@mui/icons-material";
import { MyContext } from "../../store/context";
import "./searchStyle.css";

function SearchC() {
  const { searchTodo } = useContext(MyContext);
  const [searchInput, setSearchInput] = useState("");
  return (
    <div className="searchDiv">
      <label htmlFor="inpu_search">
        <Search />
      </label>
      <Input
        className="searchInput"
        placeholder="Search Your Todos Here"
        id="inpu_search"
        // size="small"
        // variant="outlined"
        // name="search"
        value={searchInput}
        onChange={(e) => setSearchInput(e.target.value)}
      />
      <Button variant="contained" onClick={() => searchTodo(searchInput)}>
        Search
      </Button>
    </div>
  );
}

export default SearchC;
