import React, { useContext } from "react";
import { Delete, CheckCircleRounded } from "@mui/icons-material";
import { MyContext } from "../../store/context";
import "./taskStyle.css";
function Task() {
  const { todoItem, setComplated, deleteTodo } = useContext(MyContext);
  return (
    <>
      {todoItem.map((taskObj, i) => {
        return (
          <li
            className={`${taskObj.complated ? "taskLiComplated" : "taskLi"}`}
            key={i}
          >
            <span className="checkSpan">
              {taskObj.complated && <CheckCircleRounded />}
            </span>
            <input
              type="checkBox"
              id={`task_label_${taskObj.taskId}`}
              className="hiddenInp"
            />
            <label
              className="taskLabel"
              htmlFor={`task_label_${taskObj.taskId}`}
              onClick={() => setComplated(taskObj.taskId)}
            >
              {taskObj.taskName}
            </label>
            {/* <span>
              <Edit />
            </span> */}
            <span
              className="deleteSpan"
              onClick={() => deleteTodo(taskObj.taskId)}
            >
              <Delete />
            </span>
          </li>
        );
      })}
    </>
  );
}

export default Task;
