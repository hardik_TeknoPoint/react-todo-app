import React from "react";
import Search from "../components/searchC/search";
import InputC from "../components/inputC/inputC";
import Task from "../components/task/task";
function TodoPage() {
  return (
    <div>
      <Search />
      <InputC />
      <Task />
    </div>
  );
}

export default TodoPage;
