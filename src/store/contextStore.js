import { MyContext } from "./context";

import React, { useState, useEffect } from "react";

function ContextStore({ children }) {
  const [task, setTask] = useState("");
  const [Id, setId] = useState(0);
  const [todoItem, setTodoItem] = useState([]);

  const [todoList, setTodoList] = useState([
    {
      taskName: "its working",
      taskId: 321,
      complated: false,
    },
  ]);

  function setTodoItems(taskObj) {
    if (task === "") return;
    setTodoList((preValue) => [...preValue, taskObj]);
    const lastElement = todoList[todoList.length - 1];
    setId(lastElement.taskId + 1);
    setTask("");
  }

  useEffect(() => {
    setTodoItem(todoList);
  }, [todoList]);

  function setComplated(taskId) {
    // console.log(e);
    const newArr = todoList.map((ele) => {
      if (taskId === ele.taskId) return { ...ele, complated: !ele.complated };
      return ele;
    });
    setTodoList(newArr);
  }

  function deleteTodo(taskId) {
    const newArr = todoList.filter((ele) => {
      return taskId !== ele.taskId;
    });
    setTodoList(newArr);
  }

  function searchTodo(searchText) {
    // console.log("searchText======", searchText);
    const newArr = todoList.filter((ele) => {
      return ele.taskName.includes(searchText);
    });
    setTodoItem(newArr);
  }

  return (
    <MyContext.Provider
      value={{
        task,
        setTask,
        todoList,
        setTodoList,
        Id,
        setComplated,
        setTodoItems,
        deleteTodo,
        searchTodo,
        todoItem,
      }}
    >
      {children}
    </MyContext.Provider>
  );
}

export default ContextStore;
